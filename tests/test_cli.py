# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: Copyright 2023 David Seaward and contributors

from tardyon import cli


def test_invoke():
    result = cli.invoke()
    assert result is None
