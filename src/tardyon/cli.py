# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: Copyright 2023 David Seaward and contributors


def invoke():
    print("Hello world!")


if __name__ == "__main__":
    invoke()
